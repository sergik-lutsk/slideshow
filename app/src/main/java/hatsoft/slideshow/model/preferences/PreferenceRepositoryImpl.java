package hatsoft.slideshow.model.preferences;

import android.content.SharedPreferences;

import hatsoft.slideshow.MyApp;
import hatsoft.slideshow.uitils.LogUtils;

import static android.content.Context.MODE_PRIVATE;

public class PreferenceRepositoryImpl implements PreferenceRepository {
    private static final String PREF = "DB_NAME";
    private static final String DIR_PATH = "azsxdcDIR_PATH";

    // --- --- ---
    // РљР»Р°СЃСЃ РґР»СЏ СЃРѕС…СЂР°РЅРµРЅРёР№ РґР°РЅРЅС‹С…
    private SharedPreferences mPref;
    // === === ===

    public PreferenceRepositoryImpl() {
        mPref = MyApp.getContext().getSharedPreferences(PREF, MODE_PRIVATE);
    }

    @Override
    public void removeSimple(String key) {
        try {
            mPref.edit().remove(key).apply();
        } catch (Exception e) {
            LogUtils.logError(getClass().getName(), e);
        }
    }

    @Override
    public void setValueSimple(String key, Object value) {
        try {
            SharedPreferences.Editor editor = mPref.edit();
            if (value instanceof String) {
                String val = (String) value;
                editor.putString(key, val);
            }
            if (value instanceof Integer) {
                Integer val = (Integer) value;
                editor.putInt(key, val);
            }
            if (value instanceof Boolean) {
                Boolean val = (Boolean) value;
                editor.putBoolean(key, val);
            }
            editor.apply();
        } catch (Exception e) {
            LogUtils.logError(getClass().getName(), e);
        }
    }

    @Override
    public SharedPreferences getValueSimple() {
        return mPref;
    }

    @Override
    public boolean isValueExistSimple(String key) {
        try {
            return mPref.contains(key);
        } catch (Exception e) {
            LogUtils.logError(getClass().getName(), e);
            return false;
        }
    }
}
