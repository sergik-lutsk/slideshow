package hatsoft.slideshow;

import android.app.IntentService;
import android.content.Intent;

import java.util.concurrent.TimeUnit;

import hatsoft.slideshow.model.preferences.PreferenceRepository;
import hatsoft.slideshow.uitils.LogUtils;
import hatsoft.slideshow.uitils.ToasUtils;

public class MyIntentService extends IntentService {
    PreferenceRepository pref;
    boolean isRun = false;

    public MyIntentService() {
        super("myname");
    }

    public void onCreate() {
        super.onCreate();
        ToasUtils.success("Service Created");
        LogUtils.log("onCreate MyIntentService");
        isRun = true;
    }

    public void onDestroy() {
        super.onDestroy();
        ToasUtils.success("Service killed");
        LogUtils.log("onDestroy MyIntentService");
        isRun = false;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.log("Service on Start...");
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            while (isRun) {
                LogUtils.log("working...");
                TimeUnit.SECONDS.sleep(3);
            }
        } catch (InterruptedException e) {
            LogUtils.logError(getClass().getSimpleName(), e);
        }
        return;
    }
}
