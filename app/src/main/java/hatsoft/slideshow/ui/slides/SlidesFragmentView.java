package hatsoft.slideshow.ui.slides;

import android.graphics.drawable.Drawable;

import hatsoft.slideshow.ui.base.BaseView;

public interface SlidesFragmentView
{
    interface View extends BaseView.RootView {
        void setBitmap_image(Drawable bitmap_image);
    }
}
