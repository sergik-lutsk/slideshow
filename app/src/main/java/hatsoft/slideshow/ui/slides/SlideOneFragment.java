package hatsoft.slideshow.ui.slides;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import hatsoft.slideshow.R;
import hatsoft.slideshow.model.Interactor;
import hatsoft.slideshow.model.events.FragmentChangedEvent;
import hatsoft.slideshow.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SlideOneFragment extends BaseFragment implements SlidesFragmentView.View {

    Drawable bitmap_image;
    ImageView fragment_images;
    View v;

    private OnFragmentInteractionListener mListener;

    private Button btn_start;

    private Interactor.interactorSettings mInteractorSettings;

    public SlideOneFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_images_one, container, false);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        int width = displaymetrics.widthPixels;
        if (v != null) {
            fragment_images = v.findViewById(R.id.iv_fragment_images_one);
            fragment_images.setImageDrawable(bitmap_image);
        }
        return v;
    }

    @Override
    public void setBitmap_image(Drawable bitmap_image) {
        this.bitmap_image = bitmap_image;
    }

    public interface OnFragmentInteractionListener extends FragmentChangedEvent {
    }
}
