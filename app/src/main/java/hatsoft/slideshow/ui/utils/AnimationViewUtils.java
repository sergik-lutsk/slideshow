package hatsoft.slideshow.ui.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import hatsoft.slideshow.R;

public class AnimationViewUtils {

    public static void showView(Context context, View view){
        if(view.getVisibility() != View.VISIBLE){
            view.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.alpfa_show_content);
            view.startAnimation(animation);
        }
    }

    public static void hideView(Context context, final View view){
        if(view.getVisibility() == View.VISIBLE){
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.alpfa_hide_content);
            view.startAnimation(animation);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationRepeat(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                }
            });
        }
    }

    public static void changeViews(View newVew, final View oldView, Context context){
        if(newVew.getVisibility() != View.VISIBLE){
            Animation animationNew = AnimationUtils.loadAnimation(context,  R.anim.alpfa_show_content);
            Animation animationOld = AnimationUtils.loadAnimation(context, R.anim.alpfa_hide_content);

            newVew.setVisibility(View.VISIBLE);
            newVew.startAnimation(animationNew);
            oldView.startAnimation(animationOld);

            animationOld.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationRepeat(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    oldView.setVisibility(View.GONE);
                }
            });
        }
    }
}
