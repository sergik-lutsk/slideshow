package hatsoft.slideshow.ui.base;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import hatsoft.slideshow.ui.utils.fragments.FragmentChangeUtils;
import hatsoft.slideshow.ui.utils.fragments.FragmentsAnimationId;

public class BaseActivity extends AppCompatActivity {

    public void setFragment(FragmentManager fragmentManager, Fragment fragment, int layoutResIs) {
        FragmentChangeUtils.setFragment(fragmentManager, fragment, layoutResIs);
    }

    public void changeFragment(FragmentManager fragmentManager, Fragment fragment, int layoutResIs) {
        FragmentChangeUtils.changeFragment(fragmentManager, fragment, layoutResIs);
    }

    public void changeFragmentWithAnimation(FragmentManager fragmentManager, Fragment fragment, int layoutResIs, FragmentsAnimationId fragmentsAnimationId) {
        FragmentChangeUtils.changeFragmentWithAnimation(fragmentManager, fragment, layoutResIs, fragmentsAnimationId);
    }
}
