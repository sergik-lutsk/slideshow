package hatsoft.slideshow.ui.settings;

import hatsoft.slideshow.ui.base.BaseView;

public interface SettingsFragmentView {

    interface View extends BaseView.RootView {

//        void exitFromApp();
//        void openDeviceSettings();
    }

    interface Presenter extends BaseView.Presenter<View> {
//        void setDirectoryPath();
    }

}
